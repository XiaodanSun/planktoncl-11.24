import torch
import sys
import numpy as np
import os
import yaml
import matplotlib.pyplot as plt
import torchvision
from gaussian_blur import GaussianBlur
from sklearn.metrics import (precision_score, recall_score,
                             f1_score, accuracy_score, confusion_matrix)

from sklearn import metrics

from torch.utils.data import DataLoader
import torchvision.transforms as transforms
from torchvision import datasets


device = 'cuda' if torch.cuda.is_available() else 'cpu'
print("Using device:", device)


model = torchvision.models.resnet18(pretrained=False, num_classes=19).to(device)

checkpoint = torch.load('../models/all_clr_models/flipping_rotating_simclr.tar', map_location=device)
state_dict = checkpoint['state_dict']

for k in list(state_dict.keys()):
    if k.startswith('backbone.'):
        if k.startswith('backbone') and not k.startswith('backbone.fc'):
            # remove prefix
            state_dict[k[len("backbone."):]] = state_dict[k]
    del state_dict[k]

log = model.load_state_dict(state_dict, strict=False)
assert log.missing_keys == ['fc.weight', 'fc.bias']

# train_loader, test_loader
s = 1
size = 64
color_jitter = transforms.ColorJitter(0.8 * s, 0.8 * s, 0.8 * s, 0.2 * s)
data_transforms = transforms.Compose([transforms.RandomResizedCrop(size=size),
                                        transforms.RandomHorizontalFlip(),
                                        transforms.RandomApply([color_jitter], p=0.8),
                                        transforms.RandomGrayscale(p=0.2),
                                        GaussianBlur(kernel_size=int(0.1 * size)),
                                        transforms.ToTensor()])

img_data = torchvision.datasets.ImageFolder('/home/xiaodansun/work/code/my_project/processed_data/data/train/', transform=data_transforms)
train_loader = torch.utils.data.DataLoader(img_data, batch_size=8, shuffle=True, num_workers=1)
img_data = torchvision.datasets.ImageFolder('/home/xiaodansun/work/code/my_project/processed_data/data/test/', transform=data_transforms)
test_loader = torch.utils.data.DataLoader(img_data, batch_size=8, shuffle=True, num_workers=1)

# freeze all layers but the last fc
for name, param in model.named_parameters():
    if name not in ['fc.weight', 'fc.bias']:
        param.requires_grad = False

parameters = list(filter(lambda p: p.requires_grad, model.parameters()))
assert len(parameters) == 2  # fc.weight, fc.bias


optimizer = torch.optim.Adam(model.parameters(), lr=3e-5, weight_decay=0.0008)
criterion = torch.nn.CrossEntropyLoss().to(device)

def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].reshape(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res



epochs = 20
for epoch in range(epochs):
    top1_train_accuracy = 0
    count = 0
    for counter, (x_batch, y_batch) in enumerate(train_loader):
        count += 1
        x_batch = x_batch.to(device)
        y_batch = y_batch.to(device)

        logits = model(x_batch)
        loss = criterion(logits, y_batch)
        top1 = accuracy(logits, y_batch, topk=(1,))
        top1_train_accuracy += top1[0]

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    top1_accuracy = 0
    test_count = 0
    test_f1 = 0
    all_pre = []
    all_true = []
    for counter, (x_batch, y_batch) in enumerate(test_loader):
        test_count += 1
        x_batch = x_batch.to(device)
        y_batch = y_batch.to(device)

        logits = model(x_batch)
        logits =  logits.softmax(-1)
        # print(logits)

        y_test = []
        y_pro = []
        for item in logits:
            if item[0] > item[1]:
                y_test.append(0)
                y_pro.append(float(item[0]))
            else:
                y_test.append(1)
                y_pro.append(float(item[1]))
        all_true += y_test
        all_pre += y_batch.cpu().numpy().tolist()
        test_f1 += f1_score(y_batch.cpu().numpy(), np.array(y_test),  average=None)[0]

        # fpr, tpr, thresholds = metrics.roc_curve(np.array(y_batch.cpu().numpy()), np.array(y_pro))
        # print('fpr, tpr, thresholds are :')
        # print(fpr, tpr, thresholds)
        # print("***************************************")
        # test_auc = metrics.auc(fpr, tpr)
        test_cks = metrics.cohen_kappa_score(np.array(y_batch.cpu().numpy()), y_test, labels=None, weights=None, sample_weight=None)
        top1, top5 = accuracy(logits, y_batch, topk=(1,2))
        top1_accuracy += top1[0]

    top1_accuracy /= test_count
    top1_train_accuracy /= count
    test_f1 /= test_count
    #auc is: {test_auc}
    fpr, tpr, thresholds = metrics.roc_curve(all_true, all_pre)
    print(fpr, tpr)
    test_auc = metrics.auc(fpr, tpr)
    print(confusion_matrix(all_true, all_pre))
    print(f"Epoch {epoch}\tTop1 Train accuracy {top1_train_accuracy.item()}\tTop1 Test accuracy: {top1_accuracy.item()} f1 is : {test_f1}  auc is: {test_auc} Kappa Score is: {test_cks}")
