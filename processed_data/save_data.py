import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
import os

data = np.load("plankton_data_101x64_final.pkl", allow_pickle=True)

#mmkdir file
for i in range(19):
    os.mkdir(f"data/50%_train/{i}/")
    os.mkdir(f"data/50%_valid/{i}/")

#save train data

# index = 0
# for item, label in zip(data[4], data[7]):
#     item = np.array(item)
#     item = item*255
#     label = np.where(label==1)[0].item()
#     cv.imwrite(f"data/valid/{label}/{index}.png", item)
#     index += 1

# index = 0
# for item, label in zip(data[3], data[6]):
#     item = np.array(item)
#     item = item*255
#     label = np.where(label==1)[0].item()
#     cv.imwrite(f"data/train/{label}/{index}.png", item)
#     index += 1

# index = 0
# for item, label in zip(data[5], data[8]):
#     item = np.array(item)
#     item = item*255
#     label = np.where(label==1)[0].item()
#     cv.imwrite(f"data/test/{label}/{index}.png", item)
#     index += 1