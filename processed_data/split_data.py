import os
import random

path = "data/train/"
for index in range(19):
    data_path = f"{path}{index}/"
    all_path = os.listdir(data_path)
    for i in random.sample(all_path, int(0.5*len(all_path))):
        png_path = os.path.join(data_path, i)
        os.system('cp %s %s' % (png_path, f"data/50%_train/{index}/{i}")) 