import numpy as np
import pickle

from numpy import random


all_data = np.load("plankton_data_101x64_final.pkl", allow_pickle=True)
trainAttrX, valAttrX, testAttrX, trainImagesX, \
    valImagesX, testImagesX, y_train, y_val, y_test = all_data

all_data = dict()

#save all sample
for attrx, imagex, y in zip(trainAttrX, trainImagesX, y_train):
	if np.where(y==1)[0].item() not in all_data.keys():
		all_data[np.where(y==1)[0].item()] = [(attrx, imagex, y)]
	else:
		all_data[np.where(y==1)[0].item()].append((attrx, imagex, y))

for attrx, imagex, y in zip(valAttrX, valImagesX, y_val):
	if np.where(y==1)[0].item() not in all_data.keys():
		all_data[np.where(y==1)[0].item()] = [(attrx, imagex, y)]
	else:
		all_data[np.where(y==1)[0].item()].append((attrx, imagex, y))

for attrx, imagex, y in zip(testAttrX, testImagesX, y_test):
	if np.where(y==1)[0].item() not in all_data.keys():
		all_data[np.where(y==1)[0].item()] = [(attrx, imagex, y)]
	else:
		all_data[np.where(y==1)[0].item()].append((attrx, imagex, y))

#split the data
trainAttrX = []
valAttrX = [] 
testAttrX = []
trainImagesX = []
valImagesX = []
testImagesX = []
y_train = []
y_val = []
y_test = []

#shuff the data

for item in all_data.keys():
	np.random.shuffle(all_data[item])
	for index in range(len(all_data[item])):
		if index < 20:
			testAttrX.append(all_data[item][index][0])
			testImagesX.append(all_data[item][index][1])
			y_test.append(all_data[item][index][2])
		elif index < 25:
			valAttrX.append(all_data[item][index][0])
			valImagesX.append(all_data[item][index][1])
			y_val.append(all_data[item][index][2])
		else:
			trainAttrX.append(all_data[item][index][0])
			trainImagesX.append(all_data[item][index][1])
			y_train.append(all_data[item][index][2])

trainAttrX = np.array(trainAttrX)
valAttrX = np.array(valAttrX)
testAttrX = np.array(testAttrX)
trainImagesX = np.array(trainImagesX)
valImagesX = np.array(valImagesX)
testImagesX = np.array(testImagesX)
y_train = np.array(y_train)
y_val = np.array(y_val)
y_test = np.array(y_test)

with open("plankton_data_101x64_final_post.pkl", "wb") as f:
	pickle.dump((trainAttrX, valAttrX, testAttrX, trainImagesX,
					valImagesX, testImagesX, y_train, y_val, y_test),
						f, protocol=4)
